using System.Collections.Generic;
using JackieSoft;
using UnityEngine;

public class ListViewInit : MonoBehaviour
{
    [SerializeField] private ListView listView;
    [SerializeField] private int total = 1;
    [SerializeField] private int jumto = 1;

    private void OnEnable()
    {
        listView.data = new List<Cell.IData>();
        for (var i = 0; i < total; i++)
        {
            listView.data.Add(new CellData { index = i});
        }
        listView.Initialize();
    }

    [ContextMenu("FU")]
    public void FU()
    {
        listView.ScrollTo<CellData>(t => t.index == jumto, 1);
    }
    
}