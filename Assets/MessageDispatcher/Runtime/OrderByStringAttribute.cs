using System;
using System.Linq;
using System.Reflection;

namespace Jackie.Soft
{
    public class OrderByStringAttribute : Attribute
    {
        public static Comparer COMPARER = new();

        private string key;
        private int order;

        public OrderByStringAttribute(string messageKey, int executionOrder)
        {
            key = messageKey;
            order = executionOrder;
        }

        public class Comparer : Message.Dispatcher<string>.Comparer
        {
            public override int Compare(Message.ICallback x, Message.ICallback y)
            {
                if (x == null) return 1;
                if (y == null) return -1;
                var xAttribute = x.GetType().GetCustomAttributes<OrderByStringAttribute>().FirstOrDefault(att => att.key == keyComparer);
                var yAttribute = y.GetType().GetCustomAttributes<OrderByStringAttribute>().FirstOrDefault(att => att.key == keyComparer);

                if (xAttribute == null) return 1;
                if (yAttribute == null) return -1;

                return xAttribute.order.CompareTo(yAttribute.order);
            }
        }
    }
}