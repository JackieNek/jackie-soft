using System;
using System.Linq;
using System.Reflection;

namespace Jackie.Soft
{
    public class OrderByTypeAttribute : Attribute
    {
        public static Comparer COMPARER = new();

        private Type type;
        private int order;

        public OrderByTypeAttribute(Type messageType, int executionOrder)
        {
            type = messageType;
            order = executionOrder;
        }

        public class Comparer : Message.Dispatcher<Type>.Comparer
        {
            public override int Compare(Message.ICallback x, Message.ICallback y)
            {
                if (x == null) return 1;
                if (y == null) return -1;

                var xAttribute = x.GetType().GetCustomAttributes<OrderByTypeAttribute>().FirstOrDefault(att => att.type == keyComparer);
                var yAttribute = y.GetType().GetCustomAttributes<OrderByTypeAttribute>().FirstOrDefault(att => att.type == keyComparer);

                if (xAttribute == null) return 1;
                if (yAttribute == null) return -1;

                return xAttribute.order.CompareTo(yAttribute.order);
            }
        }
    }
}