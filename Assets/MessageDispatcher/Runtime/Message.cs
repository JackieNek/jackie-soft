namespace Jackie.Soft
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    public class Message
    {
        private Dictionary<Type, IDispatcher> _dispatchers = new();

        private static Message instance = new();

        public static Dispatcher<T> Use<T>() {
            var key = typeof(T);
            if (!instance._dispatchers.ContainsKey(key))
                instance._dispatchers[key] = new Dispatcher<T>();

            var dispatcher = instance._dispatchers[key];
            dispatcher.Clean();
            return dispatcher as Dispatcher<T>;
        }

        public delegate void Invoke<in T>(T callback);

        private interface IDispatcher
        {
            void Clean();
        }

        public interface ICallback { }

        public class Dispatcher<T> : IDispatcher
        {
            private Dictionary<T, List<ICallback>> _message = new();
            private ICallback                      _context;

            private T        _key;
            private Comparer _comparer;

            public Dispatcher<T> OrderBy(Comparer comparer) {
                _comparer = comparer;
                return this;
            }

            public Dispatcher<T> With<TContext>([NotNull] TContext context) where TContext : ICallback {
                _context = context;
                return this;
            }

            public Dispatcher<T> Event([NotNull] T key) {
                _key = key;
                return this;
            }

            public Dispatcher<T> Sub([NotNull] params T[] keys) {
                for (var i = 0; i < keys.Length; i++)
                    Sub(keys[i], _context);

                return this;
            }

            public Dispatcher<T> UnSub([NotNull] params T[] keys) {
                for (var i = 0; i < keys.Length; i++)
                    UnSub(keys[i], _context);

                return this;
            }

            public void Execute<TCallback>([NotNull] Invoke<TCallback> execution) {
                if (!_message.ContainsKey(_key)) return;
                var ls = _message[_key];
                if (_comparer != null) {
                    _comparer.keyComparer = _key;
                    ls.Sort(_comparer);
                }

                foreach (var message in ls)
                    execution.Invoke((TCallback)message);
            }

            private void Sub([NotNull] T key, [NotNull] ICallback context) {
                if (!_message.ContainsKey(key)) _message.Add(key, new List<ICallback>());
                if (_message[key].Contains(context)) throw new Exception($"{_context.GetType()} has been existed with {key}");
                _message[key].Add(context);
            }

            private void UnSub([NotNull] T key, [NotNull] ICallback context) {
                if (_message.ContainsKey(key)) _message[key].Remove(context);
            }

            void IDispatcher.Clean() {
                _context  = default;
                _key      = default;
                _comparer = null;
            }

            public abstract class Comparer : IComparer<ICallback>
            {
                public T keyComparer;

                public abstract int Compare(ICallback x, ICallback y);
            }
        }

        public static Type CreatedBy<T>() {
            return typeof(T);
        }


    }

    public static class MessageShortcut
    {
       
        public static K Sub<K, T>(this K context) where K : T, Message.ICallback {
            Message.Use<Type>().With(context).Sub(typeof(T));
            return context;
        }

        public static K UnSub<K, T>(this K context) where K : T, Message.ICallback {
            Message.Use<Type>().With(context).UnSub(typeof(T));
            return context;
        }

        public static void Execute<K>(Message.Invoke<K> execution) {
            Message.Use<Type>().Event(typeof(K)).Execute(execution);
        }
    }
    
}