using System;
using System.Collections;
using Jackie.Soft;
using UnityEngine;

[OrderByType(typeof(ISub), 1)]
public class Subscriber : MonoBehaviour, ISub, Message.ICallback
{
    private void OnEnable()
    {
        Message.Use<Type>().With(this).Sub(typeof(ISub), typeof(Subscriber));
        // this.Sub<Subscriber, ISub>();
    }

    private void OnDisable()
    {
        Message.Use<Type>().With(this).UnSub(typeof(ISub), typeof(Subscriber));
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);
        Debug.Log("Send message ISub");
        Message.Use<Type>().Event(typeof(ISub)).OrderBy(OrderByTypeAttribute.COMPARER).Execute<ISub>(e => e.Exe());
        yield return new WaitForSeconds(0.5f);
        Debug.Log("Send message Subscriber");
        Message.Use<Type>().Event(typeof(Subscriber)).Execute<Subscriber>(e => e.Exe());
    }

    public void Exe()
    {
        Debug.Log(typeof(Subscriber));
    }
}

public interface ISub
{
    void Exe();
}
