using System;
using Jackie.Soft;
using UnityEngine;

[OrderByType(typeof(ISub), 2)]
public class Subscriber1 : MonoBehaviour, ISub, Message.ICallback
{
    
    
    private void OnEnable()
    {
        Message.Use<Type>().With(this).Sub(typeof(ISub));
    }

    private void OnDisable()
    {
        Message.Use<Type>().With(this).UnSub(typeof(ISub));
    }
    
    public void Exe()
    {
        Debug.Log(typeof(Subscriber1));
    }
}