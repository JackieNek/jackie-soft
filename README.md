### Cài đặt bắt buộc:
- <b>"com.coffee.git-dependency-resolver":</b>  "https://github.com/mob-sakai/GitDependencyResolverForUnity.git"

### Các gói khác:
- <b>"com.jackiesoft.poolmanager":</b> "https://gitlab.com/JackieNek/jackie-soft.git?path=Assets/PoolManager"
- <b>"com.jackiesoft.messagedispatcher":</b> "https://gitlab.com/JackieNek/jackie-soft.git?path=Assets/MessageDispatcher"
- <b>"com.jackiesoft.listview":</b> "https://gitlab.com/JackieNek/jackie-soft.git?path=Assets/ListView"
- <b>"com.demigiant.dotween":</b> "https://gitlab.com/JackieNek/jackie-soft.git?path=Assets/Plugins/Demigiant"
